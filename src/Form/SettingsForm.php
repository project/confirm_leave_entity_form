<?php

namespace Drupal\confirm_leave_entity_form\Form;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Confirm Leave Entity Form settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_leave_entity_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['confirm_leave_entity_form.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * Generates the configuration form.
   * Options are: confirm in all forms, confirm only in specified forms.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['confirm_leave_checkbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Confirm leave if there are changes in the form'),
      '#weight' => -13,
      '#default_value' => $this->config('confirm_leave_entity_form.settings')->get('confirm_checkbox'),
    ];

    $form['confirm_leave_textarea'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Write forms_id to check, separated by line breaks'),
      '#weight' => -12,
      '#default_value' => $this->config('confirm_leave_entity_form.settings')->get('confirm_textarea'),
      '#states' => [
        'invisible' => [
          ':input[name="confirm_leave_checkbox"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $etm = \Drupal::entityTypeManager()->getDefinitions();

    // Generate an options list of Entity types.
    $options = [];
    foreach( $etm as $entityName => $entityDefinition) {
      $options[$entityName] = (string) $entityDefinition->getLabel();
    }


    $form['confirm_leave_entities'] = [
      '#type' => 'checkboxes',
      '#options' => array('node' => $this->t('Node'), 'media' => $this->t('Media'), 'taxonomy' => $this->t('Taxonomy'), 'user' => $this->t('User')),
      '#title' => $this->t('What entity type do u want to check?'),
      '#default_value' => $this->config('confirm_leave_entity_form.settings')->get('confirm_entities'),
      '#states' => [
        'invisible' => [
          ':input[name="confirm_leave_checkbox"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('confirm_leave_entity_form.settings')
      ->set('confirm_checkbox', $form_state->getValue('confirm_leave_checkbox'))
      ->set('confirm_textarea', $form_state->getValue('confirm_leave_textarea'))
      ->set('confirm_entities', $form_state->getValue('confirm_leave_entities'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

