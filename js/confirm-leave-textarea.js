/*
 * AL salir, se pedirá confirmación siempre de la página, excepto en el caso de
 * que se pulse cualquier botón de submit.
 *
 */

let bPreguntar = true;

window.addEventListener("beforeunload", function (event) {
  if (bPreguntar) {
    event.returnValue = "\o/";
  }
});

window.addEventListener('load', function () {

  let forms = document.forms;

  for (let i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', function () {
      bPreguntar = false;
    })
  }

});

