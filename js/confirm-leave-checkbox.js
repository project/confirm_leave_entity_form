/*
 * Al cargar la página se incia la función 'inicio'. También se añade el evento 'beforeunload' a window.
 * Dependiendo del valor de bPreguntar, se preguntará confirmación al salir, o no.
 *
 */

window.addEventListener('load', inicio);
window.addEventListener("beforeunload", function (event) {
  if (bPreguntar) {
    event.returnValue = "\o/";
  }
});

var bPreguntar = false;

function inicio() {

  if (document.getElementsByClassName("form-submit").length > 0) {
    document.addEventListener('keypress', cambiar);
  }

  let forms = document.forms;

  for (let i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', function () {
      bPreguntar = false;
    })
  }

}

function cambiar() {
  bPreguntar = true;
}

